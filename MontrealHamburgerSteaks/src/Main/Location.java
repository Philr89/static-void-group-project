package Main;

public class Location {
	
	private int rent;
	private String address;
	private String locationDimensions;
	private int equipmentRental;
	private int water;
	private int electricity;
	private Earning earning;
	
	
	


	public Location(int rent, String address, String locationDimensions, int equipmentRental, int water,
			int electricity, Earning earning) {
		this.rent = rent;
		this.address = address;
		this.locationDimensions = locationDimensions;
		this.equipmentRental = equipmentRental;
		this.water = water;
		this.electricity = electricity;
		this.earning= earning;
	}

	public void setMonthlyRent(int rent) {
		this.rent = rent;
	}
	
	public int getRent() {
		return rent;
	}

	public String getAddress() {
		return address;
	}

	public String getLocationDimensions() {
		return locationDimensions;
	}

	public int getEquipmentRental() {
		return equipmentRental;
	}

	public int getWater() {
		return water;
	}

	public int getElectricity() {
		return electricity;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public void setBuildingDimensions(String dimensions) {
		this.locationDimensions = dimensions;
	}
	
	public void setCostForEquipmentRental(int costOfRental) {
		this.equipmentRental = costOfRental;		
	}
	public Earning getEarning() {
		return earning;
	}

	public void setEarning(Earning earning) {
		this.earning = earning;
	}
	
	public void setUtilitiesCost(int water, int electricity) {
		this.water = water;
		this.electricity = electricity;
	}
	public double costPerMonth() {
		return this.rent+this.electricity+this.equipmentRental+this.water;
	}
	

}

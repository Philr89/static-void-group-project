package Main;

public class Supply extends Inventory {
    double costPerUnit;

    public double getCostPerUnit() {
		return costPerUnit;
	}

	public void setCostPerUnit(double costPerUnit) {
		this.costPerUnit = costPerUnit;
	}

	public Supply(String item, double costPerUnit, Location location) {
        super(item,location);
        this.costPerUnit = costPerUnit;
    }
}

package Main;

import java.util.ArrayList;
import java.util.List;

public class RestoMTL {
    //Main method for the project
	
	


    public static void main(String[] args) {
    	Location location = new Location(1000,"1234 Street Ave.","800'X400'",500,1000,2500, new Earning(0,20000,0));
    	List<Employee> empList= new ArrayList<Employee>();
    	empList.add(new Employee("Sally", "May", 5521, "Manager", "1234 Street Ave.","person@example.com", "(123)-555-5555", "123123123"));
    	empList.add(new Employee("Sally", "May", 5521, "Cashier", "1234 Street Ave.","person@example.com", "(123)-555-5555", "123123123"));
    	empList.add(new Employee("Sally", "May", 5521, "Busboy", "1234 Street Ave.","person@example.com", "(123)-555-5555", "123123123"));
    	empList.add(new Employee("Sally", "May", 5521, "Waiter", "1234 Street Ave.","person@example.com", "(123)-555-5555", "123123123"));
    	double inventoryCost = 0;
    	Bean bean = new Bean("Bean", 3.25, 30,location);
    	inventoryCost += bean.inventoryCost();
        Ketchup ketchup = new Ketchup("Ketchup", 4.35, 45,location);
        inventoryCost += ketchup.inventoryCost();
        Meat meat = new Meat("Meat", 4.70, 60,location);
        inventoryCost += meat.inventoryCost();
        Oil oil = new Oil("Oil", 2.70, 56,location);
        inventoryCost += oil.inventoryCost();
        Potato potato = new Potato("Potato", 1.24, 30.50,location);
        inventoryCost += potato.inventoryCost();
        Rice rice = new Rice("Rice", 4.37, 120.80,location);
        inventoryCost += rice.inventoryCost();
        Tomato tomato = new Tomato("Tomato", 3.65, 18.00,location);
        inventoryCost += tomato.inventoryCost();
        
        DishWashingLiquid dishWashingLiquid = new DishWashingLiquid("Dishwashing Liquid", 8.99, 25,location);
        inventoryCost += dishWashingLiquid.inventoryCost();
        FacialTissue facialTissue = new FacialTissue("Facial Tissue", 3.45, 67,location);
        inventoryCost += facialTissue.inventoryCost();
        HandSanitizer handSanitizer = new HandSanitizer("Hand Sanitizer", 5.60, 35,location);
        inventoryCost += handSanitizer.inventoryCost();
        PlasticBag plasticBag = new PlasticBag("Plastic Bag", 0.10, 1200,location);
        inventoryCost += plasticBag.inventoryCost();
        ScrubbingSponge scrubbingSponge = new ScrubbingSponge("Scrubbing Sponge", 0.45, 87,location);
        inventoryCost += scrubbingSponge.inventoryCost();
        Soap soap = new Soap("Soap", 3.89, 50,location);
        inventoryCost += soap.inventoryCost();
        ToiletTowel toiletTowel = new ToiletTowel("Toilet Towel", 6.30, 110,location);
        inventoryCost += toiletTowel.inventoryCost();
        
        location.getEarning().setInventory(inventoryCost);
        location.getEarning().setInventory(location.costPerMonth());
        
    	GetName.getName(bean, ketchup, meat, oil, potato, rice, tomato,dishWashingLiquid, facialTissue, handSanitizer,plasticBag, scrubbingSponge, soap, toiletTowel);
    	Menu.serveBurger(meat,tomato,location.getEarning());
    	

    	
    }
}

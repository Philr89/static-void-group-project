package Main;

public class Tomato extends FoodWeight {
    double weight;

    public Tomato(String item, double costPerKilo, double weight, Location location) {
        super(item, costPerKilo, location);
        this.weight = weight;
    }
    
    
    public void addWeight(double weight) {
		this.weight += weight;
		this.location.getEarning().addInventory(weight*this.costPerKilo);
	}
    public void subWeight(double weight) {
		this.weight -= weight;
		this.location.getEarning().removeInventory(weight*this.costPerKilo);
		
	}
    


	public double getWeight() {
        return weight;
    }
	public double inventoryCost() {
		return this.costPerKilo*this.weight;
	}
}

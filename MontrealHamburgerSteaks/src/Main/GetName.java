package Main;

import java.util.Scanner;

public class GetName {
    public static void getName(Bean bean, Ketchup ketchup, Meat meat,Oil oil, Potato potato, Rice rice, Tomato tomato, DishWashingLiquid dishWashingLiquid, FacialTissue facialTissue,
    		HandSanitizer handSanitizer,PlasticBag plasticBag,ScrubbingSponge scrubbingSponge,Soap soap,ToiletTowel toiletTowel ) {
// Enter the name of items by user
        Scanner item = new Scanner(System.in);
        System.out.println("Enter your item name:");
        String itemName = item.nextLine();
        
        if (itemName.equals("Bean")) {
            System.out.println("Enter the weight of bean:");
            double weightOfBean = item.nextDouble();
            bean.addWeight(weightOfBean);
            System.out.println("Enter the cost of bean per kilogram:");
            double costOfBean = item.nextDouble();
            System.out.println("Total bean in stock: " + (bean.getWeight()) + " kilograms.");
            System.out.println("Payment for beans: " + (weightOfBean * costOfBean));
            bean.location.getEarning().costofBoughtInventory(weightOfBean * costOfBean);

        } else if (itemName.equals("Ketchup")) {
            System.out.println("Enter the amount of ketchup:");
            int amountOfKetchup = item.nextInt();
            ketchup.addunit(amountOfKetchup);            
            System.out.println("Enter the cost of each ketchup:");
            double costOfKetchup = item.nextDouble();
            System.out.println("Total ketchup in stock: " + (ketchup.getAmount()) + " tubes.");
            System.out.printf("Payment for ketchup: " + (amountOfKetchup * costOfKetchup));
            ketchup.location.getEarning().costofBoughtInventory(amountOfKetchup * costOfKetchup);

        }  else if (itemName.equals("Meat")) {
            System.out.println("Enter the weight of meat:");
            double weightOfMeat = item.nextDouble();
            meat.addWeight(weightOfMeat);
            System.out.println("Enter the cost of meat per kilogram:");
            double costOfMeat = item.nextDouble();
            System.out.println("Total meat in stock: " + (meat.getWeight()) + " kilograms.");
            System.out.println("Payment for meat: " + (weightOfMeat * costOfMeat));
            meat.location.getEarning().costofBoughtInventory(weightOfMeat * costOfMeat);
            
        }else if (itemName.equals("Oil")) {
            System.out.println("Enter the amount of oil:");
            int amountOfOil = item.nextInt();
            oil.addunit(amountOfOil);
            System.out.println("Enter the cost of each oil:");
            double costOfOil = item.nextDouble();
            System.out.println("Total oil in stock: " + ( oil.getAmount()) + " bottles.");
            System.out.printf("Payment for oil: " + (amountOfOil * costOfOil));
            oil.location.getEarning().costofBoughtInventory(amountOfOil * costOfOil);

        } else if (itemName.equals("Potato")) {
            System.out.println("Enter the weight of potato:");
            double weightOfPotato = item.nextDouble();
            potato.addWeight(weightOfPotato);
            System.out.println("Enter the cost of potato per kilogram:");
            double costOfPotato = item.nextDouble();
            System.out.println("Total potato in stock: " + (potato.getWeight()) + " kilograms.");
            System.out.println("Payment for potato: " + (weightOfPotato * costOfPotato));
            potato.location.getEarning().costofBoughtInventory(weightOfPotato * costOfPotato);
            
        } else if (itemName.equals("Rice")) {
            System.out.println("Enter the weight of rice:");
            double weightOfRice = item.nextDouble();
            rice.addWeight(weightOfRice);
            System.out.println("Enter the cost of rice per kilogram:");
            double costOfRice = item.nextDouble();
            System.out.println("Total rice in stock: " + (rice.getWeight()) + " kilograms.");
            System.out.println("Payment for rice: " + (weightOfRice * costOfRice));
            rice.location.getEarning().costofBoughtInventory(weightOfRice * costOfRice);
            
            
        } else if (itemName.equals("Tomato")) {
            System.out.println("Enter the weight of tomato:");
            double weightOfTomato = item.nextDouble();
            tomato.addWeight(weightOfTomato);
            System.out.println("Enter the cost of tomato per kilogram:");
            double costOfTomato = item.nextDouble();
            System.out.println("Total tomato in stock: " + (tomato.getWeight()) + " kilograms.");
            System.out.println("Payment for tomato: " + (weightOfTomato * costOfTomato));
            tomato.location.getEarning().costofBoughtInventory(weightOfTomato * costOfTomato);
            
            
        } else if (itemName.equals("Dishwashing Liquid")) {
            System.out.println("Enter the amount of dishwashing liquid:");
            int amountOfDish = item.nextInt();
            dishWashingLiquid.addunit(amountOfDish);
            System.out.println("Enter the cost of each dishwashing liquid:");
            double costOfDish = item.nextDouble();
            System.out.println("Total dishwashing liquid in stock: " + (dishWashingLiquid.getAmount()) + " boxes.");
            System.out.printf("Payment for dishwashing liquid: " + (amountOfDish * costOfDish));
            dishWashingLiquid.location.getEarning().costofBoughtInventory(amountOfDish * costOfDish);

        } else if (itemName.equals("Facial Tissue")) {
            System.out.println("Enter the amount of facial tissue:");
            int amountOfTissue = item.nextInt();
            facialTissue.addunit(amountOfTissue);
            System.out.println("Enter the cost of each facial tissue:");
            double costOfTissue = item.nextDouble();
            System.out.println("Total facial tissue in stock: " + (facialTissue.getAmount()) + " boxes.");
            System.out.printf("Payment for facial tissue: " + (amountOfTissue * costOfTissue));
            dishWashingLiquid.location.getEarning().costofBoughtInventory(amountOfTissue * costOfTissue);

        } else if (itemName.equals("Hand Sanitizer")) {
            System.out.println("Enter the amount of hand sanitizer:");
            int amountOfHand = item.nextInt();
            handSanitizer.addunit(amountOfHand);
            System.out.println("Enter the cost of each hand sanitizer:");
            double costOfHand = item.nextDouble();
            System.out.println("Total hand sanitizer in stock: " + (handSanitizer.getAmount()) + " tubes.");
            System.out.printf("Payment for hand sanitizer: " + (amountOfHand * costOfHand));
            handSanitizer.location.getEarning().costofBoughtInventory(amountOfHand * costOfHand);
            

        } else if (itemName.equals("Plastic Bag")) {
            System.out.println("Enter the amount of plastic bag:");
            int amountOfPlastic = item.nextInt();
            plasticBag.addunit(amountOfPlastic);
            System.out.println("Enter the cost of each plastic bag:");
            double costOfPlastic = item.nextDouble();
            System.out.println("Total plastic bag in stock: " + (plasticBag.getAmount()) + " bags.");
            System.out.printf("Payment for plastic bag: " + (amountOfPlastic * costOfPlastic));
            plasticBag.location.getEarning().costofBoughtInventory(amountOfPlastic * costOfPlastic);

        } else if (itemName.equals("Scrubbing Sponge")) {
            System.out.println("Enter the amount of scrubbing sponge:");
            int amountOfSponge = item.nextInt();
            scrubbingSponge.addunit(amountOfSponge);
            System.out.println("Enter the cost of each scrubbing sponge:");
            double costOfSponge = item.nextDouble();
            System.out.println("Total scrubbing sponge in stock: " + (scrubbingSponge.getAmount()) + " packs.");
            System.out.printf("Payment for scrubbing sponge: " + (amountOfSponge * costOfSponge));
            scrubbingSponge.location.getEarning().costofBoughtInventory(amountOfSponge * costOfSponge);

        } else if (itemName.equals("Soap")) {
            System.out.println("Enter the amount of soap:");
            int amountOfSoap = item.nextInt();
            soap.addunit(amountOfSoap);
            System.out.println("Enter the cost of each soap:");
            double costOfSoap = item.nextDouble();
            System.out.println("Total soap in stock: " + (soap.getAmount()) + " boxes.");
            System.out.printf("Payment for soap: " + (amountOfSoap * costOfSoap));
            soap.location.getEarning().costofBoughtInventory(amountOfSoap * costOfSoap);

        } else if (itemName.equals("Toilet Towel")) {
            System.out.println("Enter the amount of toilet towel:");
            int amountOfTowel = item.nextInt();
            toiletTowel.addunit(amountOfTowel);
            System.out.println("Enter the cost of each toilet towel:");
            double costOfTowel = item.nextDouble();
            System.out.println("Total toilet towel in stock: " + (toiletTowel.getAmount()) + " boxes.");
            System.out.printf("Payment for toilet towel: " + (amountOfTowel * costOfTowel));
            toiletTowel.location.getEarning().costofBoughtInventory(amountOfTowel * costOfTowel);

        } else {
        	System.out.println("Please enter the name of item correctly.");
        }

        

        System.out.println("\nThe items in inventory is as follow:\n\n" + "FOOD:");
        System.out.printf("%s %.2f kilograms, at the cost of: %.2f dollars.\n", bean.item, bean.weight, bean.costPerKilo * bean.weight);
        System.out.printf("%s %d tubes, at the cost of: %.2f dollars.\n", ketchup.item, ketchup.amount, ketchup.costPerUnit * ketchup.amount);
        System.out.printf("%s %.2f kilograms, at the cost of: %.2f dollars.\n", meat.item, meat.weight, meat.costPerKilo * meat.weight);
        System.out.printf("%s %d bottles, at the cost of: %.2f dollars.\n", oil.item, oil.amount, oil.costPerUnit * oil.amount);
        System.out.printf("%s %.2f kilograms, at the cost of: %.2f dollars.\n", potato.item, potato.weight, potato.costPerKilo * potato.weight);
        System.out.printf("%s %.2f kilograms, at the cost of: %.2f dollars.\n", rice.item, rice.weight, rice.costPerKilo * rice.weight);
        System.out.printf("%s %.2f kilograms, at the cost of: %.2f dollars.\n", tomato.item, tomato.weight, tomato.costPerKilo * tomato.weight);
       
        System.out.println("SUPPLY: \n");
        System.out.printf("%s %d boxes, at the cost of: %.2f dollars.\n", dishWashingLiquid.item, dishWashingLiquid.amount, dishWashingLiquid.costPerUnit * dishWashingLiquid.amount);
        System.out.printf("%s %d boxes, at the cost of: %.2f dollars.\n", facialTissue.item, facialTissue.amount, facialTissue.costPerUnit * facialTissue.amount);
        System.out.printf("%s %d tubes, at the cost of: %.2f dollars.\n", handSanitizer.item, handSanitizer.amount, handSanitizer.costPerUnit * handSanitizer.amount);
        System.out.printf("%s %d bags, at the cost of: %.2f dollars.\n", plasticBag.item, plasticBag.amount, plasticBag.costPerUnit * plasticBag.amount);
        System.out.printf("%s %d packs, at the cost of: %.2f dollars.\n", scrubbingSponge.item, scrubbingSponge.amount, scrubbingSponge.costPerUnit * scrubbingSponge.amount);
        System.out.printf("%s %d boxes, at the cost of: %.2f dollars.\n", soap.item, soap.amount, soap.costPerUnit * soap.amount);
        System.out.printf("%s %d boxes, at the cost of: %.2f dollars.\n", toiletTowel.item, toiletTowel.amount, toiletTowel.costPerUnit * toiletTowel.amount);
    }
}


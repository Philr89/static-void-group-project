package Main;

public class DishWashingLiquid extends Supply{
    int amount;

    public int getAmount() {
		return amount;
	}
	public DishWashingLiquid(String item, double costPerUnit, int amount, Location location) {
        super(item, costPerUnit, location);
        this.amount = amount;
    }
	public double inventoryCost() {
		return this.costPerUnit*this.amount;
	}
    public void addunit(double unit) {
		this.amount += unit;
		this.location.getEarning().addInventory(unit*this.costPerUnit);
	}
    public void subunit(double unit) {
		this.amount -= unit;
		this.location.getEarning().removeInventory(unit*this.costPerUnit);
		
	}
}

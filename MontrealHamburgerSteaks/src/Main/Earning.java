package Main;

public class Earning {
	double inventory;
	double liquidAssets;
	double monthlyBills;
	public Earning(double in, double liquid, double bills) {
		this.inventory = in;
		this.liquidAssets=liquid;
		this.monthlyBills=bills;
	}
	public double getInventory() {
		return inventory;
	}
	public void setInventory(double inventory) {
		this.inventory = inventory;
	}
	public double getLiquidAssets() {
		return liquidAssets;
	}
	public void setLiquidAssets(double liquidAssets) {
		this.liquidAssets = liquidAssets;
	}
	public double getMonthlyBills() {
		return monthlyBills;
	}
	public void setMonthlyBills(double monthlyBills) {
		this.monthlyBills = monthlyBills;
	}
	public void isProfitable() {
		if(this.liquidAssets+this.inventory>this.monthlyBills) {
			System.out.println("This location can turn a profit.");
		}else {
			System.out.println("This location can not return a profit");
		}
	}
	public void removeInventory(double foodCost) {
		this.inventory-= foodCost;
	}
	public void addInventory(double cost) {
		this.inventory+=cost;
	}
	public void foodSold(double moneyEarned) {
		this.liquidAssets+= moneyEarned;
	}
	public void costofBoughtInventory(double cost) {
		this.liquidAssets-= cost;
	}
	
}

package Main;

public class FoodItem extends Inventory{
    double costPerUnit;

    public double getCostPerUnit() {
		return costPerUnit;
	}

	public void setCostPerUnit(double costPerUnit) {
		this.costPerUnit = costPerUnit;
	}

	public FoodItem(String item, double costPerUnit, Location location) {
        super(item, location);
        this.costPerUnit = costPerUnit;
    }
}

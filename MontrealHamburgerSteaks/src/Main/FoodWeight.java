package Main;

public class FoodWeight extends Inventory{
    double costPerKilo;

    public FoodWeight(String item, double costPerKilo, Location location) {
        super(item, location);
        this.costPerKilo = costPerKilo;
    }

	public double getCostPerKilo() {
		return costPerKilo;
	}

	public void setCostPerKilo(double costPerKilo) {
		this.costPerKilo = costPerKilo;
	}
}

package Main;

public class Menu {
	
	public static void serveBurger(Meat hamburgermeat, Tomato tomato, Earning earning ) {
		tomato.subWeight(.25);
		hamburgermeat.subWeight(.5);
		earning.foodSold(3.50);
		System.out.println("You have been served a delicious hamburger");
	}

}

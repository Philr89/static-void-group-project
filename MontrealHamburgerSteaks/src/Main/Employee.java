package Main;

public class Employee {
	public Employee (String firstName, String familyName, int employeeId, String employeeRole, String homeAddress,String email,String phoneNumber, String sin) { 
	
		this.firstName = firstName;
		this.familyName = familyName;
		this.employeeId = employeeId;
		this.employeeRole = employeeRole;
		this.homeAddress = homeAddress;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.sin = sin;
		if(employeeRole.equals("Manager")) {
			this.hourlyRate=25;
		}else if(employeeRole.equals("Cook")) {
			this.hourlyRate=15;
		}else if(employeeRole.equals("Cashier")) {
			this.hourlyRate=11.5;
		}else if(employeeRole.equals("Waiter")) {
			this.hourlyRate=9.25;
		}else if(employeeRole.equals("Busboy")) {
			this.hourlyRate=25;
		}
	}

	String firstName;
	String familyName;
	int employeeId;
	String employeeRole;
	String homeAddress;
	String email;
	String phoneNumber;
	String sin;
	double hourlyRate;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeRole() {
		return employeeRole;
	}
	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}
	public String getHomeAddress() {
		return homeAddress;
	}
	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getSin() {
		return sin;
	}

	
}
